CC = g++

all: a.out

a.out: calc.tab.c
	$(CC) calc.tab.c

calc.tab.c: calc.y
	bison calc.y

clean:
	rm calc.tab.c
	rm a.out
