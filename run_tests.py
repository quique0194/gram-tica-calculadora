import os

os.system('cat tests_in.txt | ./a.out > out.txt')

out = open('out.txt', 'r')
tests_out = open('tests_out.txt', 'r')

errors = []

success = 0
fail = 0

for line, (a, b) in enumerate(zip(out, tests_out)):
    a, b = a[:-1], b[:-1]       # strip \n
    if a == b:
        if a:
            success += 1
            print '.',
    else:
        fail += 1
        print 'E',
        errors.append((line+1, a, b))

print ''
print 'Run {} tests, {} succeded and {} failed:'.format(success + fail,
                                                        success,
                                                        fail)

for error in errors:
    print 'Line', str(error[0])+':', error[1], '!=', error[2]

os.system('rm out.txt')
