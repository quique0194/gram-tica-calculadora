/* Prologue */

%{
    #include <stdio.h>
    #include <math.h>
    int yylex();
    void yyerror(const char*);
%}



/* Bison declarations */

%define api.value.type {double}
%token NUM

%% /* Grammar rules and actions */

input: 
    %empty
|   input line
;

line:
    '\n'                        { printf("\n"); }
|   A '\n'                      { printf("%.10g\n", $1); }
|   error '\n'                // Error recovery
;


A:
    B AP                        { $$ = $1 + $2; }
;

AP:
    '+' A                       { $$ = $2; }
|   '-' A                       { $$ = -$2; }
|   %empty                      { $$ = 0; }
;

B:
    C BP                        { $$ = $1 * $2; }
;

BP:
    '*' B                       { $$ = $2; }
|   '/' B                       {   
                                    if ($2 == 0) {
                                        yyerror("division by zero");
                                        YYERROR;
                                    }
                                    $$ = 1.0 / $2;
                                }
|   %empty                      { $$ = 1.0; }
;

C:
   '-' C                       { $$ = -$2; }
|   D CP                        { 
                                    if( isnan(pow($1, $2)) ) {
                                        yyerror("invalid pow");
                                        YYERROR;
                                    }
                                    $$ = pow($1, $2);
                                }
;

CP:
    '^' C                       { $$ = $2; }
|   %empty                      { $$ = 1.0; }
;

D:
    '(' A ')'                   { $$ = $2; }
|   '[' A ']'                   { $$ = $2; }
|   's' 'i' 'n' '(' A ')'       { $$ = sin($5); }
|   'c' 'o' 's' '(' A ')'       { $$ = cos($5); }
|   'l' 'n' '(' A ')' {
                                    if ( $4 == 0 || isnan(log($4)) ) {
                                        yyerror("invalid log");
                                        YYERROR;
                                    }
                                    $$ = log($4);
                                }
|   NUM                         { $$ = $1; }
;



%% /* Epilogue */

/* 
    The lexical analyzer returns a double floating point
    number on the stack and the token NUM, or the numeric code
    of the character read if not a number.  It skips all blanks
    and tabs, and returns 0 for end-of-input.
*/

#include <ctype.h>

int yylex() {
    char c = getchar();

    /* skip whitespace */
    while( c == ' ' || c == '\t'){
        c = getchar();
    }

    /* Process numbers */
    if (c == '.' || isdigit(c) ) {
        ungetc(c, stdin);
        scanf("%lf", &yylval);
        return NUM;
    }

    if (c == EOF) {
        return 0;
    }

    return c;
}


int main() {
    return yyparse();
}

#include <stdio.h>

void yyerror(const char* s) {
    printf("ERROR: %s\n", s);
}
